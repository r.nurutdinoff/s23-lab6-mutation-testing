from bonus_system import calculateBonuses


standardMultiplier= 0.5
premiumMultiplier = 0.1
diamondMultiplier = 0.2


def test_standart_bonus():
    assert calculateBonuses("Standard", -1) == standardMultiplier * 1
    assert calculateBonuses("Standard", 1) == standardMultiplier * 1
    assert calculateBonuses("Standard", 9999) == standardMultiplier * 1
    assert calculateBonuses("Standard", 10000) == standardMultiplier * 1.5
    assert calculateBonuses("Standard", 10001) == standardMultiplier * 1.5
    assert calculateBonuses("Standard", 49999) == standardMultiplier * 1.5
    assert calculateBonuses("Standard", 50000) == standardMultiplier * 2
    assert calculateBonuses("Standard", 50001) == standardMultiplier * 2
    assert calculateBonuses("Standard", 99999) == standardMultiplier * 2
    assert calculateBonuses("Standard", 100000) == standardMultiplier * 2.5
    assert calculateBonuses("Standard", 100001) == standardMultiplier * 2.5

def test_premium_bonus():
    assert calculateBonuses("Premium", -1) == premiumMultiplier * 1
    assert calculateBonuses("Premium", 1) == premiumMultiplier * 1
    assert calculateBonuses("Premium", 9999) == premiumMultiplier * 1
    assert calculateBonuses("Premium", 10000) == premiumMultiplier * 1.5
    assert calculateBonuses("Premium", 10001) == premiumMultiplier * 1.5
    assert calculateBonuses("Premium", 49999) == premiumMultiplier * 1.5
    assert calculateBonuses("Premium", 50000) == premiumMultiplier * 2
    assert calculateBonuses("Premium", 50001) == premiumMultiplier * 2
    assert calculateBonuses("Premium", 99999) == premiumMultiplier * 2
    assert calculateBonuses("Premium", 100000) == premiumMultiplier * 2.5
    assert calculateBonuses("Premium", 100001) == premiumMultiplier * 2.5
    
def test_diamond_bonus():
    assert calculateBonuses("Diamond", -1) == diamondMultiplier * 1
    assert calculateBonuses("Diamond", 1) == diamondMultiplier * 1
    assert calculateBonuses("Diamond", 9999) == diamondMultiplier * 1
    assert calculateBonuses("Diamond", 10000) == diamondMultiplier * 1.5
    assert calculateBonuses("Diamond", 10001) == diamondMultiplier * 1.5
    assert calculateBonuses("Diamond", 49999) == diamondMultiplier * 1.5
    assert calculateBonuses("Diamond", 50000) == diamondMultiplier * 2
    assert calculateBonuses("Diamond", 50001) == diamondMultiplier * 2
    assert calculateBonuses("Diamond", 99999) == diamondMultiplier * 2
    assert calculateBonuses("Diamond", 100000) == diamondMultiplier * 2.5
    assert calculateBonuses("Diamond", 100001) == diamondMultiplier * 2.5

def test_invalid_input():
    assert calculateBonuses("stub", -1) == 0
    assert calculateBonuses("stub", 1) == 0
    assert calculateBonuses("stub", 9999) == 0
    assert calculateBonuses("stub", 10000) == 0
    assert calculateBonuses("stub", 10001) == 0
    assert calculateBonuses("stub", 49999) == 0
    assert calculateBonuses("stub", 50000) == 0
    assert calculateBonuses("stub", 50001) == 0
    assert calculateBonuses("stub", 99999) == 0
    assert calculateBonuses("stub", 100000) == 0
    assert calculateBonuses("stub", 100001) == 0
    assert calculateBonuses("stub", 13000) == 0